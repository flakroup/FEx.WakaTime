﻿namespace FEx.WakaTime.Models
{
    public class DurationProject
    {
        public string Name { get; set; }
        public double TotalSeconds { get; set; }
    }
}